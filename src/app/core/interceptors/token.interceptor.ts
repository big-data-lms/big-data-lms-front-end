import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, filter, switchMap, take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private auth: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addTokenHeader(req)).pipe(
      catchError(
        (error: HttpErrorResponse) => this.handleAuthError(error, req, next)
      )
    );
  }

  private addTokenHeader(req: HttpRequest<any>): HttpRequest<any> {
    if (this.auth.isAuthenticated()) {
      if (!req.url.includes('auth/')) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${this.auth.accessToken}`
          }
        });
      }
    }
    return req;
  }

  private handleAuthError(error: HttpErrorResponse, req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (error.status === 401) {
      if (req.url.includes('refresh/')) {
        return this.logout(error);
      }

      if (this.refreshTokenInProgress) {
        return this.refreshTokenSubject.pipe(
          filter(result => result !== null),
          take(1),
          switchMap(() => next.handle(this.addTokenHeader(req))));
      } else {
        this.refreshTokenInProgress = true;
        this.refreshTokenSubject.next(null);
        return this.auth.loginUsingRefreshToken().pipe(
          switchMap((token: any) => {
            this.refreshTokenInProgress = false;
            this.refreshTokenSubject.next(token);
            return next.handle(this.addTokenHeader(req));
          })
        );
      }

    }

    return throwError(error);
  }

  private logout(error: HttpErrorResponse) {
    this.refreshTokenInProgress = false;
    this.auth.logout();
    this.router.navigateByUrl('/auth');
    return throwError(error);
  }
}
