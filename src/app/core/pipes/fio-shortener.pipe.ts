import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fioShortener'
})
export class FioShortenerPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const splittedFio: Array<string> = value.split(' ');
    return splittedFio[0] + ' ' +
      splittedFio
        .slice(1)
        .map(word => word[0].toUpperCase() + '.')
        .join('');
  }

}
