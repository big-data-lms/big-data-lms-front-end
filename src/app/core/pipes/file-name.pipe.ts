import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fileName'
})
export class FileNamePipe implements PipeTransform {

  transform(value: string, args?: any): string {
    const result = new RegExp('^.+/([^/]+)$').exec(value);
    return decodeURI(result[1]);
  }

}
