import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientXsrfModule} from '@angular/common/http';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {ApiHostInterceptor} from './interceptors/api-host.interceptor';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {TruncatePipe} from './pipes/truncate.pipe';
import {FioShortenerPipe} from './pipes/fio-shortener.pipe';
import {FloorPipe} from './pipes/floor.pipe';
import {FileNamePipe} from './pipes/file-name.pipe';
import {LoadingDirective} from './directives/loading.directive';
import {LoadingComponent} from './loading/loading.component';
import {DynamicComponentAnchorDirective} from './directives/dynamic-component-anchor.directive';
import {ReplaceLineBreaksPipe} from './pipes/replace-line-breaks.pipe';

const providers = [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    },
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: ApiHostInterceptor
    }
  ];

@NgModule({
  declarations: [
    TruncatePipe,
    FioShortenerPipe,
    FloorPipe,
    FileNamePipe,
    LoadingDirective,
    LoadingComponent,
    DynamicComponentAnchorDirective,
    ReplaceLineBreaksPipe
  ],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatProgressBarModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    }),
  ],
  exports: [
    TruncatePipe,
    FioShortenerPipe,
    FloorPipe,
    FileNamePipe,
    ReplaceLineBreaksPipe,
    LoadingDirective,
    LoadingComponent,
    DynamicComponentAnchorDirective
  ],
  entryComponents: [LoadingComponent]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [...providers]
    };
  }
}
