import { TestBed } from '@angular/core/testing';

import { SnackBarService } from './snack-bar.service';
import {MatSnackBarModule} from '@angular/material';

describe('SnackBarService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MatSnackBarModule
    ],
    providers: [
      SnackBarService
    ]
  }));

  it('should be created', () => {
    const service: SnackBarService = TestBed.get(SnackBarService);
    expect(service).toBeTruthy();
  });
});
