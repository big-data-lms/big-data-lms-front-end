import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileDownloadService {

  constructor() { }

  downloadFile(fileName: string, fileContent) {
    const blob = new Blob([fileContent], { type: 'application/x-yaml' });
    const blobUrl = window.URL.createObjectURL(blob);

    const a = document.createElement('a');
    document.body.appendChild(a);

    a.href = blobUrl;
    a.download = fileName;
    a.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(blobUrl);
      document.body.removeChild(a);
    }, 0);
  }

  downloadFileFromForm(form, formFieldName: string, fileExtension: string) {
    const fileContent = form.value[formFieldName];
    this.downloadFile(`${formFieldName}.${fileExtension}`, fileContent);
  }

  onFileChange($event, fileField: string, form) {
    if ($event.target.files.length > 0) {
      const file = $event.target.files[0];
      form.patchValue({
        [fileField]: file
      });
    }
  }

}
