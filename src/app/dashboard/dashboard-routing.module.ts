import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardLayoutComponent} from './dashboard-layout/dashboard-layout.component';
import {lmsCoreRoutes} from './lms-core/lms-core-routing.module';
import {personalizedLearningRoutes} from './personalized-learning/personalized-learning-routing.module';


const routes: Routes = [
  {
    path: '', component: DashboardLayoutComponent, children: [
      ...lmsCoreRoutes,
      ...personalizedLearningRoutes
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
