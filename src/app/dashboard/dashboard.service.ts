import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  public readonly title$: Subject<string> = new BehaviorSubject('');

  constructor() {}

}
