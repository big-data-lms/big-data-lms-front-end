import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralFormForAdaptiveTestsComponent } from './general-form-for-adaptive-tests/general-form-for-adaptive-tests.component';
import { CreateAdaptiveTestComponent } from './create-adaptive-test/create-adaptive-test.component';
import { EditAdaptiveTestComponent } from './edit-adaptive-test/edit-adaptive-test.component';
import { QuestionGraphComponent } from './question-graph/question-graph.component';
import {MaterialModule} from '../../material.module';
import {CoreModule} from '../../../core/core.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import { EditQuestionFormComponent } from './edit-question-form/edit-question-form.component';
import { EditAnswerOptionFormComponent } from './edit-answer-option-form/edit-answer-option-form.component';
import { ListOfAdaptiveTestsComponent } from './list-of-adaptive-tests/list-of-adaptive-tests.component';
import { CardOfAdaptiveTestComponent } from './card-of-adaptive-test/card-of-adaptive-test.component';
import { TestPassingCurrentQuestionComponent } from './test-passing-current-question/test-passing-current-question.component';

const components = [
  GeneralFormForAdaptiveTestsComponent,
  CreateAdaptiveTestComponent,
  EditAdaptiveTestComponent,
  QuestionGraphComponent,
  EditQuestionFormComponent,
  EditAnswerOptionFormComponent,
  ListOfAdaptiveTestsComponent,
  CardOfAdaptiveTestComponent,
  TestPassingCurrentQuestionComponent
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxGraphModule
  ],
  exports: [
    ...components
  ]
})
export class AdaptiveTestsModule { }
