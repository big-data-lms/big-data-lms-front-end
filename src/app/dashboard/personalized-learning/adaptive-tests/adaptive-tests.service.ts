import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {
  AdaptiveTest,
  AdaptiveTestForm,
  AnswerOption,
  AnswerOptionCreate,
  LearningPath,
  Question,
  TestPassing
} from '../classes';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdaptiveTestsService {

  adaptiveTestGeneralFormEmitter$: Subject<AdaptiveTestForm> = new Subject<AdaptiveTestForm>();
  currentAdaptiveTest$: BehaviorSubject<AdaptiveTest> = new BehaviorSubject<AdaptiveTest>(new AdaptiveTest());
  currentQuestionList$: BehaviorSubject<Question[]> = new BehaviorSubject<Question[]>([]);
  currentEditQuestion$: BehaviorSubject<Question> = new BehaviorSubject<Question>(new Question());

  constructor(private http: HttpClient) { }

  createAdaptiveTest(form: AdaptiveTestForm) {
    return this.http.post('/api/adaptive_tests/', form);
  }

  getInfoAboutAdaptiveTest(adaptiveTestId: number): Observable<AdaptiveTest> {
    return this.http.get<AdaptiveTest>(`/api/adaptive_tests/${adaptiveTestId}`);
  }

  editLearningPath(id: number, form: AdaptiveTestForm) {
    return this.http.patch(`/api/adaptive_tests/${id}/`, form);
  }

  getQuestionList(adaptiveTestId: number): Observable<Question[]> {
    const params = new HttpParams().set('adaptive_test', String(adaptiveTestId));
    return this.http.get<Question[]>(`/api/questions/`, {params});
  }

  onLearningPathSearch(name: string) {
    const params = new HttpParams().set('name', name);
    return this.http.get<LearningPath[]>(`/api/learning_paths`, {params});
  }

  editAnswerOption(answerOptionId: number,
                   requestBody: { next_question: number; text: string; learning_path_id: number }) {
    return this.http.patch(`/api/answer_options/${answerOptionId}/`, requestBody);
  }

  editQuestion(questionId: number, requestBody: { text: string }) {
    return this.http.patch(`/api/questions/${questionId}/`, requestBody);
  }

  createQuestion(requestBody: { adaptive_test: number; text: string }) {
    return this.http.post(`/api/questions/`, requestBody);
  }

  getAdaptiveTestList(): Observable<AdaptiveTest[]> {
    return this.http.get<AdaptiveTest[]>('/api/adaptive_tests/');
  }

  startPassingTest(adaptiveTestId: number): Observable<TestPassing> {
    return this.http.post<TestPassing>('/api/test_passings/', {
      test_id: adaptiveTestId
    });
  }

  getCurrentQuestion(testPassingId: number): Observable<Question> {
    return this.http.get<Question>(`/api/test_passings/${testPassingId}/get_current_question/`);
  }

  getTestPassingInfo(testPassingId: number): Observable<TestPassing> {
    return this.http.get<TestPassing>(`/api/test_passings/${testPassingId}/`);
  }

  writeAnswer(testPassingId: number, requestBody: {answer_options: number}) {
    return this.http.post<number>(`/api/test_passings/${testPassingId}/write_answer/`, requestBody);
  }

  createAnswerOption(requestBody: AnswerOptionCreate): Observable<AnswerOption> {
    return this.http.post<AnswerOption>(`/api/answer_options/`, requestBody);
  }

  deleteAdaptiveTest(adaptiveTestId: number) {
    return this.http.delete(`/api/adaptive_tests/${adaptiveTestId}`);
  }

}
