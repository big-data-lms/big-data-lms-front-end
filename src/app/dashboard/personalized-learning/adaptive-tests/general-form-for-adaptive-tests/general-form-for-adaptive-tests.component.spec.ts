import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralFormForAdaptiveTestsComponent } from './general-form-for-adaptive-tests.component';

describe('GeneralFormForAdaptiveTestsComponent', () => {
  let component: GeneralFormForAdaptiveTestsComponent;
  let fixture: ComponentFixture<GeneralFormForAdaptiveTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralFormForAdaptiveTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralFormForAdaptiveTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
