import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdaptiveTestForm} from '../../classes';
import {AdaptiveTestsService} from '../adaptive-tests.service';

@Component({
  selector: 'app-general-form-for-adaptive-tests',
  templateUrl: './general-form-for-adaptive-tests.component.html',
  styleUrls: ['./general-form-for-adaptive-tests.component.scss']
})
export class GeneralFormForAdaptiveTestsComponent implements OnInit {

  form: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(80)]],
    description: ['', [Validators.required, Validators.maxLength(500)]],
  });

  @Input()
  set formInput(formValue: AdaptiveTestForm) {
    this.form.patchValue({...formValue});
  }

  constructor(private formBuilder: FormBuilder,
              private adaptiveTestsService: AdaptiveTestsService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.adaptiveTestsService.adaptiveTestGeneralFormEmitter$.next(this.form.value);
  }

}
