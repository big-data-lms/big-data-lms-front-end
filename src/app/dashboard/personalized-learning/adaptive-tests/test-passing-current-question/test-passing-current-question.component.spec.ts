import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestPassingCurrentQuestionComponent } from './test-passing-current-question.component';

describe('TestPassingCurrentQuestionComponent', () => {
  let component: TestPassingCurrentQuestionComponent;
  let fixture: ComponentFixture<TestPassingCurrentQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestPassingCurrentQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPassingCurrentQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
