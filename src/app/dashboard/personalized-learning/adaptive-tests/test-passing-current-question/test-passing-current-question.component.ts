import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../../dashboard.service';
import {Observable} from 'rxjs';
import {map, switchMap, take} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {Question, TestPassing} from '../../classes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-test-passing-current-question',
  templateUrl: './test-passing-current-question.component.html',
  styleUrls: ['./test-passing-current-question.component.scss']
})
export class TestPassingCurrentQuestionComponent implements OnInit {

  testPassingId$: Observable<number> = this.route.params
    .pipe(
      map(params => params.id)
    );
  currentQuestion: Question = new Question();
  testPassing: TestPassing = new TestPassing();

  form: FormGroup = this.formBuilder.group({
    answer_options: ['', [Validators.required, Validators.min(1)]],
  });

  constructor(
    private dashboardService: DashboardService,
    private adaptiveTestsService: AdaptiveTestsService,
    private snackBarService: SnackBarService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Прохождение теста');
    this.getCurrentQuestion();
    this.getTestPassingInfo();
  }

  getTestPassingInfo() {
    this.testPassingId$
      .pipe(
        take(1),
        switchMap(testPassingId => this.adaptiveTestsService.getTestPassingInfo(testPassingId)),
      )
      .subscribe(testPassing => {
        this.testPassing = testPassing;
        this.dashboardService.title$.next(`Прохождение теста - ${this.testPassing.test.name}`);
      });
  }

  getCurrentQuestion() {
    this.testPassingId$
      .pipe(
        take(1),
        switchMap(testPassingId => this.adaptiveTestsService.getCurrentQuestion(testPassingId)),
      )
      .subscribe(question => this.currentQuestion = question);
  }

  onSubmit() {
    const requestBody = this.form.value;
    this.adaptiveTestsService.writeAnswer(this.testPassing.id, requestBody)
      .pipe(
        take(1)
      )
      .subscribe(newQuestionId => {
        if (newQuestionId === null) {
          this.snackBarService.openSnackBar('Персональная рекомендация сформирована');
          this.router.navigateByUrl('/dashboard/learning_paths');
        } else {
          this.getCurrentQuestion();
        }
      });
  }

}
