import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfAdaptiveTestsComponent } from './list-of-adaptive-tests.component';

describe('ListOfAdaptiveTestsComponent', () => {
  let component: ListOfAdaptiveTestsComponent;
  let fixture: ComponentFixture<ListOfAdaptiveTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfAdaptiveTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfAdaptiveTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
