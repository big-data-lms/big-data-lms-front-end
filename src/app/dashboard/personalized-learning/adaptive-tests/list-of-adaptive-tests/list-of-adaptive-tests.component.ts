import { Component, OnInit } from '@angular/core';
import {AdaptiveTest} from '../../classes';
import {DashboardService} from '../../../dashboard.service';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-list-of-adaptive-tests',
  templateUrl: './list-of-adaptive-tests.component.html',
  styleUrls: ['./list-of-adaptive-tests.component.scss']
})
export class ListOfAdaptiveTestsComponent implements OnInit {

  adaptiveTestList: AdaptiveTest[] = [];

  constructor(
    private dashboardService: DashboardService,
    private adaptiveTestsService: AdaptiveTestsService
  ) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Список тестов');
    this.getAdaptiveTestItems();
  }

  private getAdaptiveTestItems() {
    this.adaptiveTestsService.getAdaptiveTestList()
      .pipe(
        take(1)
      )
      .subscribe(adaptiveTestList => this.adaptiveTestList = adaptiveTestList);
  }

}
