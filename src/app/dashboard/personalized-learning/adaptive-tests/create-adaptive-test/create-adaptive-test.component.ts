import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {DashboardService} from '../../../dashboard.service';
import {switchMap, takeUntil} from 'rxjs/operators';
import {AdaptiveTestsService} from '../adaptive-tests.service';

@Component({
  selector: 'app-create-adaptive-test',
  templateUrl: './create-adaptive-test.component.html',
  styleUrls: ['./create-adaptive-test.component.scss']
})
export class CreateAdaptiveTestComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private adaptiveTestsService: AdaptiveTestsService,
    private router: Router,
    private snackBarService: SnackBarService,
    private dashboardService: DashboardService
  ) {
    this.dashboardService.title$.next('Создание адаптивного теста');
  }

  ngOnInit(): void {
    this.createAdaptiveTestOnEmit();
  }

  createAdaptiveTestOnEmit() {
    this.adaptiveTestsService.adaptiveTestGeneralFormEmitter$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap(form => this.adaptiveTestsService.createAdaptiveTest(form))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Тест создан');
        this.router.navigateByUrl('/dashboard/adaptive_tests');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
