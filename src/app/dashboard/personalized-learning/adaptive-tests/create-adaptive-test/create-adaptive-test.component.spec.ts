import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAdaptiveTestComponent } from './create-adaptive-test.component';

describe('CreateAdaptiveTestComponent', () => {
  let component: CreateAdaptiveTestComponent;
  let fixture: ComponentFixture<CreateAdaptiveTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAdaptiveTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdaptiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
