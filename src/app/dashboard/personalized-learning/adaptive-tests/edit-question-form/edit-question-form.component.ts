import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormBuilder, Validators} from '@angular/forms';
import {AnswerOption, AnswerOptionCreate, Question} from '../../classes';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {take, takeUntil} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-edit-question-form',
  templateUrl: './edit-question-form.component.html',
  styleUrls: ['./edit-question-form.component.scss']
})
export class EditQuestionFormComponent implements OnInit, OnDestroy {

  form = this.formBuilder.group({
    text: ['', [Validators.required, Validators.maxLength(500)]],
  });
  question: Question = new Question();
  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private adaptiveTestsService: AdaptiveTestsService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit(): void {
    this.getCurrentQuestion();
  }

  getCurrentQuestion() {
    this.adaptiveTestsService.currentEditQuestion$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(currentEditQuestion => {
        this.form.patchValue({text: currentEditQuestion.text});
        this.question = currentEditQuestion;
      });
  }

  saveQuestion() {
    const formValue = this.form.value;
    this.adaptiveTestsService.editQuestion(this.question.id, {text: formValue.text})
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Текст вопроса изменён');
      });
  }

  createAnswerOption() {
    const requestBody = new AnswerOptionCreate(
      this.question.id,
    );
    this.adaptiveTestsService.createAnswerOption(requestBody)
      .pipe(
        take(1)
      )
      .subscribe(answerOption => {
        this.snackBarService.openSnackBar('Вариант ответа создан');
        this.question.answer_options.push(answerOption);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
