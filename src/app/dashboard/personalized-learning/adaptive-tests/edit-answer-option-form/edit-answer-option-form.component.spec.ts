import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAnswerOptionFormComponent } from './edit-answer-option-form.component';

describe('EditAnswerOptionFormComponent', () => {
  let component: EditAnswerOptionFormComponent;
  let fixture: ComponentFixture<EditAnswerOptionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAnswerOptionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAnswerOptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
