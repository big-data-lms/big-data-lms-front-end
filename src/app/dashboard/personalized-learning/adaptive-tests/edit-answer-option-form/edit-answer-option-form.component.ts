import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, zip} from 'rxjs';
import {FormBuilder, Validators} from '@angular/forms';
import {AnswerOption, LearningPath, Question} from '../../classes';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {debounceTime, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-edit-answer-option-form',
  templateUrl: './edit-answer-option-form.component.html',
  styleUrls: ['./edit-answer-option-form.component.scss']
})
export class EditAnswerOptionFormComponent implements OnInit, OnDestroy {

  currentQuestionList$: Subject<Question[]> = this.adaptiveTestsService.currentQuestionList$;

  @Input()
  set formInput(formValue: AnswerOption) {
    this.answerOptionId = formValue.id;
    this.currentQuestionList$
      .pipe(
        take(1)
      )
      .subscribe(questionList => {
        this.form.patchValue({
          text: formValue.text,
          next_question: questionList.filter(q => q.id === formValue.next_question)[0],
          learning_path_id: formValue.learning_path
        });
      });
  }

  private unsubscribe$: Subject<any> = new Subject();
  form = this.formBuilder.group({
    text: ['', [Validators.required, Validators.maxLength(500)]],
    next_question: [''],
    learning_path_id: [''],
  });
  autoCompleteOptions: LearningPath[] = [];
  private answerOptionId: number;

  constructor(
    private formBuilder: FormBuilder,
    private adaptiveTestsService: AdaptiveTestsService,
    private cd: ChangeDetectorRef,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit(): void {
    this.onFormChanges();
  }

  onFormChanges() {
    this.form.get('learning_path_id').valueChanges
      .pipe(
        debounceTime(500),
        switchMap(value => this.adaptiveTestsService.onLearningPathSearch(value)),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(result => {
        this.autoCompleteOptions = result;
        this.cd.detectChanges();
      });
  }

  saveAnswerOption() {
    const formValue = this.form.value;
    const requestBody = {
      text: formValue.text,
      next_question: formValue.next_question?.id,
      learning_path_id: formValue.learning_path_id?.id
    };

    this.adaptiveTestsService.editAnswerOption(this.answerOptionId, requestBody)
      .pipe(
        take(1),
        switchMap(_ => zip(
          this.adaptiveTestsService.currentAdaptiveTest$,
          this.adaptiveTestsService.currentQuestionList$,
          this.adaptiveTestsService.currentEditQuestion$
        )),
        take(1),
        map(val => ({currentAdaptiveTest: val[0], currentQuestionList: val[1], currentEditQuestion: val[2]}))
      )
      .subscribe(({currentAdaptiveTest, currentQuestionList, currentEditQuestion}) => {
        const editedAnswerOption: AnswerOption = new AnswerOption(
          this.answerOptionId,
          formValue.text,
          currentEditQuestion.id,
          formValue.next_question?.id,
          formValue.learning_path_id
        );
        currentEditQuestion.answer_options = currentEditQuestion.answer_options.map(a => {
          if (a.id === this.answerOptionId) {
            return editedAnswerOption;
          }
          return a;
        });
        this.adaptiveTestsService.currentAdaptiveTest$.next(currentAdaptiveTest);
        this.currentQuestionList$.next(currentQuestionList);
        this.snackBarService.openSnackBar('Вариант ответа сохранён');
      });
  }

  createQuestion() {
    const createdQuestionResponse$ = this.adaptiveTestsService.currentAdaptiveTest$
      .pipe(
        take(1),
        map(adaptiveTest => adaptiveTest.id),
        switchMap(adaptiveTestId => {
          const requestBody = {
            text: 'Новый вопрос',
            adaptive_test: adaptiveTestId
          };
          return this.adaptiveTestsService.createQuestion(requestBody);
        }),
      );

    zip(
      createdQuestionResponse$,
      this.currentQuestionList$
    )
      .pipe(
        take(1),
        map(val => ({createdQuestion: val[0], currentQuestionList: val[1]}))
      )
      .subscribe(({createdQuestion, currentQuestionList}) => {
        currentQuestionList.push(createdQuestion as Question);
        this.currentQuestionList$.next(currentQuestionList);
        this.form.get('next_question').patchValue(createdQuestion);
        this.saveAnswerOption();
      });
  }

  valueMapper(option) {
    if (option) {
      return option.name;
    }
    return '';
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }
}
