import {Component, OnDestroy, OnInit} from '@angular/core';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {Subject, zip} from 'rxjs';
import {filter, map, takeUntil, tap} from 'rxjs/operators';
import {Question} from '../../classes';

@Component({
  selector: 'app-question-graph',
  templateUrl: './question-graph.component.html',
  styleUrls: ['./question-graph.component.scss']
})
export class QuestionGraphComponent implements OnInit, OnDestroy {

  hierarchicalGraph = {nodes: [], links: []};
  currentQuestionList: Question[] = [];
  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private adaptiveTestsService: AdaptiveTestsService
  ) {}

  ngOnInit(): void {
    this.updateGraphOnReceive();
  }

  updateGraphOnReceive() {
    zip(
      this.adaptiveTestsService.currentAdaptiveTest$,
      this.adaptiveTestsService.currentQuestionList$
    ).pipe(
        takeUntil(this.unsubscribe$),
        map(val => ({currentTest: val[0], currentQuestionList: val[1]})),
        filter(({currentTest, currentQuestionList}) => currentQuestionList.length > 0),
        tap(({currentTest, currentQuestionList}) => {
          this.hierarchicalGraph = {nodes: [], links: []};
          this.currentQuestionList = currentQuestionList;
          const firstQuestionId = currentTest.first_question;
          this.writeToGraphRecursively(firstQuestionId);
        })
      )
      .subscribe();
  }

  writeToGraphRecursively(questionId: number) {
    const question = this.currentQuestionList.filter(q => q.id === questionId)[0];
    this.hierarchicalGraph.nodes.push({id: question.id, label: question.text.substring(0, 10), question});
    for (const answer of question.answer_options) {
      if (answer.next_question) {
        this.hierarchicalGraph.links.push({source: question.id, target: answer.next_question});
        this.writeToGraphRecursively(answer.next_question);
      }
    }
  }

  onNodeClick(node) {
    this.adaptiveTestsService.currentEditQuestion$.next(node.question);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
