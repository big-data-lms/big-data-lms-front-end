import { TestBed } from '@angular/core/testing';

import { AdaptiveTestsService } from './adaptive-tests.service';

describe('AdaptiveTestsService', () => {
  let service: AdaptiveTestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdaptiveTestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
