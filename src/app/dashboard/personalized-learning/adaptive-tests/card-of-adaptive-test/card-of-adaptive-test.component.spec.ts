import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardOfAdaptiveTestComponent } from './card-of-adaptive-test.component';

describe('CardOfAdaptiveTestComponent', () => {
  let component: CardOfAdaptiveTestComponent;
  let fixture: ComponentFixture<CardOfAdaptiveTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardOfAdaptiveTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardOfAdaptiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
