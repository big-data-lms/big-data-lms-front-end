import {Component, Input, OnInit} from '@angular/core';
import {AdaptiveTest} from '../../classes';
import {AuthService} from '../../../../auth/auth.service';
import {take} from 'rxjs/operators';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-card-of-adaptive-test',
  templateUrl: './card-of-adaptive-test.component.html',
  styleUrls: ['./card-of-adaptive-test.component.scss']
})
export class CardOfAdaptiveTestComponent implements OnInit {

  @Input() adaptiveTest: AdaptiveTest;
  userSubject$ = this.authService.userSubject$;

  constructor(
    private authService: AuthService,
    private adaptiveTestsService: AdaptiveTestsService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  passTest() {
    this.adaptiveTestsService.startPassingTest(this.adaptiveTest.id)
      .pipe(
        take(1)
      )
      .subscribe(testPassing => this.router.navigateByUrl(`/dashboard/test_passings/${testPassing.id}`));
  }

}
