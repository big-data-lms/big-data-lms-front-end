import {Component, OnDestroy, OnInit} from '@angular/core';
import {AdaptiveTest} from '../../classes';
import {Observable, Subject} from 'rxjs';
import {map, switchMap, take, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {AdaptiveTestsService} from '../adaptive-tests.service';
import {DashboardService} from '../../../dashboard.service';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-edit-adaptive-test',
  templateUrl: './edit-adaptive-test.component.html',
  styleUrls: ['./edit-adaptive-test.component.scss']
})
export class EditAdaptiveTestComponent implements OnInit, OnDestroy {
  adaptiveTest: AdaptiveTest = new AdaptiveTest();
  adaptiveTestId$: Observable<number> = this.route.params
    .pipe(
      map(params => params.id)
    );
  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private adaptiveTestsService: AdaptiveTestsService,
    private dashboardService: DashboardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.getInfoAboutAdaptiveTest();
    this.getQuestionList();
    this.editAdaptiveTestOnEmit();
  }

  deleteAdaptiveTest() {
    this.adaptiveTestsService.deleteAdaptiveTest(this.adaptiveTest.id)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.router.navigateByUrl('/dashboard/adaptive_tests');
        this.snackBarService.openSnackBar('Тест удалён');
      });
  }

  private getInfoAboutAdaptiveTest() {
    this.adaptiveTestId$
      .pipe(
        take(1),
        switchMap(adaptiveTestId => this.adaptiveTestsService.getInfoAboutAdaptiveTest(adaptiveTestId))
      )
      .subscribe(adaptiveTest => {
        this.adaptiveTestsService.currentAdaptiveTest$.next(adaptiveTest);
        this.adaptiveTest = adaptiveTest;
        this.dashboardService.title$.next(`Редактирование теста - ${this.adaptiveTest.name}`);
      });
  }

  private getQuestionList() {
    this.adaptiveTestId$
      .pipe(
        take(1),
        switchMap(adaptiveTestId => this.adaptiveTestsService.getQuestionList(adaptiveTestId))
      )
      .subscribe(questionList => {
        this.adaptiveTestsService.currentQuestionList$.next(questionList);
      });
  }

  editAdaptiveTestOnEmit() {
    this.adaptiveTestsService.adaptiveTestGeneralFormEmitter$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap(form => this.adaptiveTestsService.editLearningPath(this.adaptiveTest.id, form))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Общая информация изменена');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
