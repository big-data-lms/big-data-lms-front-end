import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdaptiveTestComponent } from './edit-adaptive-test.component';

describe('EditAdaptiveTestComponent', () => {
  let component: EditAdaptiveTestComponent;
  let fixture: ComponentFixture<EditAdaptiveTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdaptiveTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdaptiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
