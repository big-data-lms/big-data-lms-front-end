import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NamedLearningPathsModule} from './named-learning-paths/named-learning-paths.module';
import {AdaptiveTestsModule} from './adaptive-tests/adaptive-tests.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NamedLearningPathsModule,
    AdaptiveTestsModule
  ],
  exports: [
    NamedLearningPathsModule,
    AdaptiveTestsModule
  ]
})
export class PersonalizedLearningModule { }
