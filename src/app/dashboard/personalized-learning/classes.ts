// tslint:disable:variable-name
// tslint:disable:no-use-before-declare


import {UserSubject} from '../../auth/classes';
import {Course} from '../lms-core/classes';
import {HttpParams} from '@angular/common/http';

export class LearningPathForm {
  constructor(
    public name: string = '',
    public description: string = ''
  ) {}
}

export class LearningPathSimple {
  constructor(
    public id: number = 0,
    public name: string = ''
  ) {}
}

export class LearningPath {
  constructor(
    public id: number = 0,
    public name: string = '',
    public description: string = '',
    public creator: UserSubject = new UserSubject(),
    public favorite_learning_paths: number[] = []
  ) {}
}

export class FavoriteLearningPath {
  constructor(
    public id: number = 0,
    public learning_path: number = 0,
  ) {}
}

export class LearningPathDependency {
  constructor(
    public id: number = 0,
    public learning_path: number = 0,
    public course: Course,
    public serial_number: number = 0
  ) {}
}

export class LearningPathDependencyForSave {
  constructor(
    public learning_path: number = 0,
    public course_id: number = 0,
    public serial_number: number = 0
  ) {}
}

export class AdaptiveTestForm {
  constructor(
    public name: string = '',
    public description: string = ''
  ) {}
}

export class AdaptiveTest {
  constructor(
    public id: number = 0,
    public name: string = '',
    public description: string = '',
    public creator: UserSubject = new UserSubject(),
    public first_question: number = 0
  ) {}
}

export class Question {
  constructor(
    public id: number = 0,
    public text: string = '',
    public answer_options: AnswerOption[] = []
  ) {}
}

export class AnswerOption {
  constructor(
    public id: number = 0,
    public text: string = '',
    public question: number = 0,
    public next_question: number = null,
    public learning_path: LearningPathSimple = null
  ) {}
}

export class AnswerOptionCreate {
  constructor(
    public question: number = 0,
    public text: string = 'Новый вариант ответа',
    public next_question: number = null,
    public learning_path_id: number = null
  ) {}
}

export class TestPassing {
  constructor(
    public id: number = 0,
    public test: AdaptiveTest = new AdaptiveTest(),
    current_question: number = 0
  ) {}
}

export class LearningPathFilterForm {
  constructor(
    public onlyFavorite: boolean = false,
    public onlyMy: boolean = false,
    public visibility: PublicitySelectionOption = VisibilitySelectionOptionsEnum.ONLYPUBLIC
  ) {}

  public toHttpParams() {
    return new HttpParams()
      .set('only_favorite', String(this.onlyFavorite))
      .set('only_my', String(this.onlyMy))
      .set('public_personal_toggle', this.visibility.shortName);
  }
}

export class PublicitySelectionOption {
  constructor(
    public shortName: string,
    public fullName: string
  ) {}
}

export class VisibilitySelectionOptionsEnum {
  static readonly ONLYPUBLIC = new PublicitySelectionOption('pu', 'Только публичные');
  static readonly ONLYPERSONAL = new PublicitySelectionOption('pe', 'Только персональные');
  static readonly ALL = new PublicitySelectionOption('a', 'Все');
}

export const visibilitySelectionOptionsList = [
  VisibilitySelectionOptionsEnum.ONLYPUBLIC,
  VisibilitySelectionOptionsEnum.ONLYPERSONAL,
  VisibilitySelectionOptionsEnum.ALL
];
