import {Component, OnDestroy, OnInit} from '@angular/core';
import {LearningPathFilterForm, VisibilitySelectionOptionsEnum, visibilitySelectionOptionsList} from '../../classes';
import {FormBuilder} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NamedLearningPathsService} from '../named-learning-paths.service';

@Component({
  selector: 'app-filter-learning-paths',
  templateUrl: './filter-learning-paths.component.html',
  styleUrls: ['./filter-learning-paths.component.scss']
})
export class FilterLearningPathsComponent implements OnInit, OnDestroy {

  visibilitySelectionOptionsList = visibilitySelectionOptionsList;
  form = this.formBuilder.group({
    onlyFavorite: [false],
    onlyMy: [false],
    visibility: [VisibilitySelectionOptionsEnum.ONLYPUBLIC]
  });
  unsubscribeSubject$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private namedLearningPathsService: NamedLearningPathsService
  ) {}

  ngOnInit(): void {
    this.onFilterChanges();
  }

  onFilterChanges() {
    this.form
      .valueChanges
      .pipe(
        takeUntil(this.unsubscribeSubject$)
      )
      .subscribe(value => {
        const formObj = new LearningPathFilterForm(
          value.onlyFavorite,
          value.onlyMy,
          value.visibility
        );
        this.namedLearningPathsService.filterChangeEmitter$.next(formObj);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject$.next();
  }

}
