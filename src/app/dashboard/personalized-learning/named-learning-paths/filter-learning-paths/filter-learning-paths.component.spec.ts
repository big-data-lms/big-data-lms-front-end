import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterLearningPathsComponent } from './filter-learning-paths.component';

describe('FilterLearningPathsComponent', () => {
  let component: FilterLearningPathsComponent;
  let fixture: ComponentFixture<FilterLearningPathsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterLearningPathsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLearningPathsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
