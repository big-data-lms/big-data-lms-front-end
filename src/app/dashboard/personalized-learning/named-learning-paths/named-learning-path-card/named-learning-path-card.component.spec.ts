import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamedLearningPathCardComponent } from './named-learning-path-card.component';

describe('NamedLearningPathCardComponent', () => {
  let component: NamedLearningPathCardComponent;
  let fixture: ComponentFixture<NamedLearningPathCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamedLearningPathCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamedLearningPathCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
