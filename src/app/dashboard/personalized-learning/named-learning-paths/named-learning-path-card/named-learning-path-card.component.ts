import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../../auth/auth.service';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {take, tap} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {LearningPath} from '../../classes';

@Component({
  selector: 'app-named-learning-path-card',
  templateUrl: './named-learning-path-card.component.html',
  styleUrls: ['./named-learning-path-card.component.scss']
})
export class NamedLearningPathCardComponent implements OnInit {

  @Input() namedLearningPath: LearningPath;
  userSubject$ = this.authService.userSubject$;

  constructor(
    private authService: AuthService,
    private snackBarService: SnackBarService,
    private namedLearningPathsService: NamedLearningPathsService
  ) { }

  ngOnInit(): void {
  }

  deleteLearningPath() {
    this.namedLearningPathsService.deleteLearningPath(this.namedLearningPath.id)
      .pipe(
        take(1),
        tap(_ => this.snackBarService.openSnackBar('Траектория удалена'))
      )
      .subscribe();
  }

  addToFavorite() {
    this.namedLearningPathsService.addLearningPathToFavorite(this.namedLearningPath.id)
      .pipe(
        take(1),
        tap(_ => this.snackBarService.openSnackBar('Добавлено в избранное')),
        tap(favoriteLearningPath => this.namedLearningPath.favorite_learning_paths.push(favoriteLearningPath.id))
      )
      .subscribe();
  }

  deleteFromFavorite() {
    this.namedLearningPathsService.deleteLearningPathFromFavorite(this.namedLearningPath.favorite_learning_paths[0])
      .pipe(
        take(1),
        tap(_ => this.snackBarService.openSnackBar('Удалено из избранного')),
        tap(_ => this.namedLearningPath.favorite_learning_paths = [])
      )
      .subscribe();
  }
}
