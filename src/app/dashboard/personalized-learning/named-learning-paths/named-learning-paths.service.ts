import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
  FavoriteLearningPath,
  LearningPath,
  LearningPathDependency,
  LearningPathDependencyForSave, LearningPathFilterForm,
  LearningPathForm
} from '../classes';
import {Observable, Subject} from 'rxjs';
import {Course} from '../../lms-core/classes';

@Injectable({
  providedIn: 'root'
})
export class NamedLearningPathsService {
  public readonly namedLearningPathGeneralFormEmitter$: Subject<LearningPathForm> = new Subject<LearningPathForm>();
  public readonly filterChangeEmitter$: Subject<LearningPathFilterForm> = new Subject<LearningPathFilterForm>();

  constructor(private http: HttpClient) { }

  deleteLearningPath(id: number) {
    return this.http.delete(`/api/learning_paths/${id}`);
  }

  addLearningPathToFavorite(id: number): Observable<FavoriteLearningPath> {
    return this.http.post<FavoriteLearningPath>('/api/favorite_learning_paths/', {
      learning_path: id
    });
  }

  deleteLearningPathFromFavorite(favoriteLearningPathId: number): Observable<any> {
    return this.http.delete<any>(`/api/favorite_learning_paths/${favoriteLearningPathId}`);
  }

  getLearningPathList(filterForm?: LearningPathFilterForm): Observable<LearningPath[]> {
    const params = filterForm ? filterForm.toHttpParams() : new LearningPathFilterForm().toHttpParams();
    return this.http.get<LearningPath[]>('/api/learning_paths', {
      params
    });
  }

  getInfoAboutLearningPath(learningPathId: number): Observable<LearningPath> {
    return this.http.get<LearningPath>(`/api/learning_paths/${learningPathId}`);
  }

  getLearningPathDependencies(learningPathId: number): Observable<LearningPathDependency[]> {
    const params = new HttpParams().set('learning_path', String(learningPathId));
    return this.http.get<LearningPathDependency[]>('/api/learning_path_dependencies/', {
      params
    });
  }

  createLearningPath(form: LearningPathForm) {
    return this.http.post('/api/learning_paths/', form);
  }

  editLearningPath(learningPathId: number, form: LearningPathForm) {
    return this.http.patch(`/api/learning_paths/${learningPathId}/`, form);
  }

  onCourseSearch(searchName: string): Observable<Course[]> {
    const params = new HttpParams().set('search', searchName);
    return this.http.get<Course[]>('/api/courses', {
      params
    });
  }

  saveDependencies(learningPathId: number, learningPathDependencyList: LearningPathDependencyForSave[]) {
    return this.http.put(`/api/learning_paths/${learningPathId}/write_dependencies/`, {
      learning_path_dependencies: learningPathDependencyList
    });
  }
}
