import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamedLearningPathListComponent } from './named-learning-path-list.component';

describe('NamedLearningPathListComponent', () => {
  let component: NamedLearningPathListComponent;
  let fixture: ComponentFixture<NamedLearningPathListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamedLearningPathListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamedLearningPathListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
