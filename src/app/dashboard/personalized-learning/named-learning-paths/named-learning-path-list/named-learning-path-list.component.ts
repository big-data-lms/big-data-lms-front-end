import {Component, OnDestroy, OnInit} from '@angular/core';
import {LearningPath} from '../../classes';
import {DashboardService} from '../../../dashboard.service';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-named-learning-path-list',
  templateUrl: './named-learning-path-list.component.html',
  styleUrls: ['./named-learning-path-list.component.scss']
})
export class NamedLearningPathListComponent implements OnInit, OnDestroy {

  learningPathList: LearningPath[] = [];
  unsubscribeSubject$ = new Subject();

  constructor(
    private namedLearningPathsService: NamedLearningPathsService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit(): void {
    this.dashboardService.title$.next('Список именованных траекторий');
    this.getLearningPathItems();
    this.onFilterChanges();
  }

  onFilterChanges() {
    this.namedLearningPathsService.filterChangeEmitter$
      .pipe(
        takeUntil(this.unsubscribeSubject$),
        switchMap(filterForm => this.namedLearningPathsService.getLearningPathList(filterForm)),
        tap(learningPathList => this.learningPathList = learningPathList)
      )
      .subscribe();
  }

  getLearningPathItems() {
    this.namedLearningPathsService.getLearningPathList()
      .pipe(
        take(1),
        tap(learningPathList => this.learningPathList = learningPathList)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject$.next();
  }

}
