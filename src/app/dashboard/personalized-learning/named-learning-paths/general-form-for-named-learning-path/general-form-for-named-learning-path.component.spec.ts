import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralFormForNamedLearningPathComponent } from './general-form-for-named-learning-path.component';

describe('GeneralFormForNamedLearningPathComponent', () => {
  let component: GeneralFormForNamedLearningPathComponent;
  let fixture: ComponentFixture<GeneralFormForNamedLearningPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralFormForNamedLearningPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralFormForNamedLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
