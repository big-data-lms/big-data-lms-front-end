import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {LearningPathForm} from '../../classes';

@Component({
  selector: 'app-general-form-for-named-learning-path',
  templateUrl: './general-form-for-named-learning-path.component.html',
  styleUrls: ['./general-form-for-named-learning-path.component.scss']
})
export class GeneralFormForNamedLearningPathComponent implements OnInit {

  form: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(80)]],
    description: ['', [Validators.required, Validators.maxLength(500)]],
  });

  @Input()
  set formInput(formValue: LearningPathForm) {
    this.form.patchValue({...formValue});
  }

  constructor(private formBuilder: FormBuilder,
              private namedLearningPathsService: NamedLearningPathsService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.namedLearningPathsService.namedLearningPathGeneralFormEmitter$.next(this.form.value);
  }

}
