import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificNamedLearningPathComponent } from './specific-named-learning-path.component';

describe('SpecificNamedLearningPathComponent', () => {
  let component: SpecificNamedLearningPathComponent;
  let fixture: ComponentFixture<SpecificNamedLearningPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificNamedLearningPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificNamedLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
