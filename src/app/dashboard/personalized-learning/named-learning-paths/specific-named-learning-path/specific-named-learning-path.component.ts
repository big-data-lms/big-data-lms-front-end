import { Component, OnInit } from '@angular/core';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {DashboardService} from '../../../dashboard.service';
import {LearningPath, LearningPathDependency} from '../../classes';
import {Observable} from 'rxjs';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {AuthService} from '../../../../auth/auth.service';

@Component({
  selector: 'app-specific-named-learning-path',
  templateUrl: './specific-named-learning-path.component.html',
  styleUrls: ['./specific-named-learning-path.component.scss']
})
export class SpecificNamedLearningPathComponent implements OnInit {

  learningPathDependencies: LearningPathDependency[] = [];
  learningPath: LearningPath;
  learningPathId$: Observable<number> = this.route.params
    .pipe(
      map(params => params.id)
    );
  userSubject$ = this.authService.userSubject$;

  constructor(
    private namedLearningPathsService: NamedLearningPathsService,
    private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getInfoAboutLearningPath();
    this.getLearningPathDependencies();
  }

  getInfoAboutLearningPath() {
    this.learningPathId$
      .pipe(
        take(1),
        switchMap(learningPathId => this.namedLearningPathsService.getInfoAboutLearningPath(learningPathId))
      )
      .subscribe(learningPath => {
        learningPath.favorite_learning_paths = learningPath.favorite_learning_paths.filter(f => !!f);
        this.learningPath = learningPath;
        this.dashboardService.title$.next(`Траектория - ${this.learningPath.name}`);
      });
  }

  private getLearningPathDependencies() {
    this.learningPathId$
      .pipe(
        take(1),
        switchMap(learningPathId => this.namedLearningPathsService.getLearningPathDependencies(learningPathId))
      )
      .subscribe(learningPathDependencies => {
        this.learningPathDependencies = learningPathDependencies.sort((a, b) => a.serial_number - b.serial_number);
      });
  }

  addToFavorite() {
    this.namedLearningPathsService.addLearningPathToFavorite(this.learningPath.id)
      .pipe(
        take(1),
        tap(_ => this.snackBarService.openSnackBar('Добавлено в избранное')),
        tap(favoriteLearningPath => this.learningPath.favorite_learning_paths.push(favoriteLearningPath.id))
      )
      .subscribe();
  }

  deleteFromFavorite() {
    this.namedLearningPathsService.deleteLearningPathFromFavorite(this.learningPath.favorite_learning_paths[0])
      .pipe(
        take(1),
        tap(_ => this.snackBarService.openSnackBar('Удалено из избранного')),
        tap(_ => this.learningPath.favorite_learning_paths = [])
      )
      .subscribe();
  }

}
