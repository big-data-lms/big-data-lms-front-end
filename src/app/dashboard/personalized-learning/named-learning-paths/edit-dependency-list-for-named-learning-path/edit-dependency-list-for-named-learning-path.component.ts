import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {LearningPath, LearningPathDependency, LearningPathDependencyForSave} from '../../classes';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {Subject} from 'rxjs';
import {FormBuilder, Validators} from '@angular/forms';
import {debounceTime, switchMap, take, takeUntil} from 'rxjs/operators';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {Course} from '../../../lms-core/classes';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-edit-dependency-list-for-named-learning-path',
  templateUrl: './edit-dependency-list-for-named-learning-path.component.html',
  styleUrls: ['./edit-dependency-list-for-named-learning-path.component.scss']
})
export class EditDependencyListForNamedLearningPathComponent implements OnInit {

  @Input() learningPathDependencyList: LearningPathDependency[] = [];
  @Input() learningPath: LearningPath;
  form = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(70)]],
  });
  autoCompleteOptions: Course[] = [];
  private unsubscribeSubject$ = new Subject();


  constructor(private formBuilder: FormBuilder,
              private namedLearningPathsService: NamedLearningPathsService,
              private cd: ChangeDetectorRef,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.onFormChanges();
  }

  drop(event: CdkDragDrop<string[]>) {
    const previousIndex = event.previousIndex;
    const currentIndex = event.currentIndex;

    const draggedDependency = this.learningPathDependencyList[previousIndex];
    if (previousIndex > currentIndex) {
      this.learningPathDependencyList.splice(previousIndex, 1);
      this.learningPathDependencyList.splice(currentIndex, 0, draggedDependency);
    } else {
      this.learningPathDependencyList.splice(currentIndex + 1, 0, draggedDependency);
      this.learningPathDependencyList.splice(previousIndex, 1);
    }
    this.learningPathDependencyList.forEach((q, i) => {
      q.serial_number = i;
    });
  }

  saveDependencies() {
    const transformedDependencyList = this.learningPathDependencyList
      .map(dependency => {
        return new LearningPathDependencyForSave(
          dependency.learning_path,
          dependency.course.id,
          dependency.serial_number);
      });

    this.namedLearningPathsService.saveDependencies(this.learningPath.id, transformedDependencyList)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Данные сохранены');
      });
  }

  onFormChanges() {
    this.form.valueChanges
      .pipe(
        debounceTime(500),
        switchMap(value => this.namedLearningPathsService.onCourseSearch(value.name)),
        takeUntil(this.unsubscribeSubject$)
      )
      .subscribe(result => {
        this.autoCompleteOptions = result;
        this.cd.detectChanges();
      });

  }

  onAddCourseSubmit() {
    let submitValue = this.form.value.name;
    if (typeof submitValue === 'string' || submitValue instanceof String) {
      submitValue = this.autoCompleteOptions.filter(o => o.name === submitValue)[0];
    }
    if (submitValue === undefined) {
      this.snackBarService.openSnackBar('Введите данные существующего курса');
    } else {
      this.learningPathDependencyList
        .push(new LearningPathDependency(-1,
          this.learningPath.id,
          submitValue,
          this.learningPathDependencyList.length
        ));
      this.snackBarService.openSnackBar('Курс добавлен, не забудьте сохранить изменения');
    }
  }

  valueMapper(option) {
    return option.name;
  }

  deleteDependency(i: number) {
    this.learningPathDependencyList.splice(i, 1);
  }
}
