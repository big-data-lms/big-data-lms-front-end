import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDependencyListForNamedLearningPathComponent } from './edit-dependency-list-for-named-learning-path.component';

describe('EditDependencyListForNamedLearningPathComponent', () => {
  let component: EditDependencyListForNamedLearningPathComponent;
  let fixture: ComponentFixture<EditDependencyListForNamedLearningPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDependencyListForNamedLearningPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDependencyListForNamedLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
