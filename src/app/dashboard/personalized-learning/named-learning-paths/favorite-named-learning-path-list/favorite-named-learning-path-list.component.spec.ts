import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteNamedLearningPathListComponent } from './favorite-named-learning-path-list.component';

describe('FavoriteNamedLearningPathListComponent', () => {
  let component: FavoriteNamedLearningPathListComponent;
  let fixture: ComponentFixture<FavoriteNamedLearningPathListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteNamedLearningPathListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteNamedLearningPathListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
