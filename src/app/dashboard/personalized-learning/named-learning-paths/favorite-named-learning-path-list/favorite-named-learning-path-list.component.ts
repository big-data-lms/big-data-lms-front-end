import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorite-named-learning-path-list',
  templateUrl: './favorite-named-learning-path-list.component.html',
  styleUrls: ['./favorite-named-learning-path-list.component.scss']
})
export class FavoriteNamedLearningPathListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
