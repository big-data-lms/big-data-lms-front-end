import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNamedLearningPathComponent } from './edit-named-learning-path.component';

describe('EditNamedLearningPathComponent', () => {
  let component: EditNamedLearningPathComponent;
  let fixture: ComponentFixture<EditNamedLearningPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNamedLearningPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditNamedLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
