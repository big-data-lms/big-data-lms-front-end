import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, switchMap, take, takeUntil} from 'rxjs/operators';
import {LearningPath, LearningPathDependency} from '../../classes';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {DashboardService} from '../../../dashboard.service';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-edit-named-learning-path',
  templateUrl: './edit-named-learning-path.component.html',
  styleUrls: ['./edit-named-learning-path.component.scss']
})
export class EditNamedLearningPathComponent implements OnInit, OnDestroy {

  learningPathDependencies: LearningPathDependency[] = [];
  learningPath: LearningPath;
  learningPathId$: Observable<number> = this.route.params
    .pipe(
      map(params => params.id)
    );
  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private namedLearningPathsService: NamedLearningPathsService,
    private dashboardService: DashboardService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit(): void {
    this.getInfoAboutLearningPath();
    this.getLearningPathDependencies();
    this.editNamedLearningPathOnEmit();
  }

  getInfoAboutLearningPath() {
    this.learningPathId$
      .pipe(
        take(1),
        switchMap(learningPathId => this.namedLearningPathsService.getInfoAboutLearningPath(learningPathId))
      )
      .subscribe(learningPath => {
        learningPath.favorite_learning_paths = learningPath.favorite_learning_paths.filter(f => !!f);
        this.learningPath = learningPath;
        this.dashboardService.title$.next(`Редактирование траектории - ${this.learningPath.name}`);
      });
  }

  private getLearningPathDependencies() {
    this.learningPathId$
      .pipe(
        take(1),
        switchMap(learningPathId => this.namedLearningPathsService.getLearningPathDependencies(learningPathId))
      )
      .subscribe(learningPathDependencies => {
        this.learningPathDependencies = learningPathDependencies.sort((a, b) => a.serial_number - b.serial_number);
      });
  }

  editNamedLearningPathOnEmit() {
    this.namedLearningPathsService.namedLearningPathGeneralFormEmitter$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap(form => this.namedLearningPathsService.editLearningPath(this.learningPath.id, form))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Общая информация изменена');
      });
  }

  deleteLearningPath() {
    this.namedLearningPathsService.deleteLearningPath(this.learningPath.id)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Траектория удалена');
        this.router.navigateByUrl('/dashboard/learning_paths');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }
}
