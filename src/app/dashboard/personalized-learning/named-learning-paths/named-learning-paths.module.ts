import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NamedLearningPathListComponent } from './named-learning-path-list/named-learning-path-list.component';
import { SpecificNamedLearningPathComponent } from './specific-named-learning-path/specific-named-learning-path.component';
import { NamedLearningPathCardComponent } from './named-learning-path-card/named-learning-path-card.component';
import { FavoriteNamedLearningPathListComponent } from './favorite-named-learning-path-list/favorite-named-learning-path-list.component';
// tslint:disable-next-line:max-line-length
import { NamedLearningPathDependencyCardComponent } from './named-learning-path-dependency-card/named-learning-path-dependency-card.component';
import {MaterialModule} from '../../material.module';
import {CoreModule} from '../../../core/core.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {CourseListModule} from '../../lms-core/course-list/course-list.module';
import { CreateNamedLearningPathComponent } from './create-named-learning-path/create-named-learning-path.component';
import { EditNamedLearningPathComponent } from './edit-named-learning-path/edit-named-learning-path.component';
// tslint:disable-next-line:max-line-length
import { GeneralFormForNamedLearningPathComponent } from './general-form-for-named-learning-path/general-form-for-named-learning-path.component';
// tslint:disable-next-line:max-line-length
import { EditDependencyListForNamedLearningPathComponent } from './edit-dependency-list-for-named-learning-path/edit-dependency-list-for-named-learning-path.component';
import {ReactiveFormsModule} from '@angular/forms';
import { FilterLearningPathsComponent } from './filter-learning-paths/filter-learning-paths.component';

const components = [
  NamedLearningPathListComponent,
  SpecificNamedLearningPathComponent,
  NamedLearningPathCardComponent,
  FavoriteNamedLearningPathListComponent,
  NamedLearningPathDependencyCardComponent,
  CreateNamedLearningPathComponent,
  EditNamedLearningPathComponent,
  GeneralFormForNamedLearningPathComponent,
  EditDependencyListForNamedLearningPathComponent,
  FilterLearningPathsComponent
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    CourseListModule
  ],
  exports: [
    ...components
  ]
})
export class NamedLearningPathsModule { }
