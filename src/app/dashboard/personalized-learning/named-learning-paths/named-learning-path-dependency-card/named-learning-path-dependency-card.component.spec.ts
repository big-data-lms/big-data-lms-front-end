import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamedLearningPathDependencyCardComponent } from './named-learning-path-dependency-card.component';

describe('NamedLearningPathDependencyCardComponent', () => {
  let component: NamedLearningPathDependencyCardComponent;
  let fixture: ComponentFixture<NamedLearningPathDependencyCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamedLearningPathDependencyCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamedLearningPathDependencyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
