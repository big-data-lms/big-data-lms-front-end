import {Component, Input, OnInit} from '@angular/core';
import {LearningPathDependency} from '../../classes';

@Component({
  selector: 'app-named-learning-path-dependency-card',
  templateUrl: './named-learning-path-dependency-card.component.html',
  styleUrls: ['./named-learning-path-dependency-card.component.scss']
})
export class NamedLearningPathDependencyCardComponent implements OnInit {

  @Input() learningPathDependency: LearningPathDependency;

  constructor() { }

  ngOnInit(): void {
  }

}
