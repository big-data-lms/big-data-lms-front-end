import { TestBed } from '@angular/core/testing';

import { NamedLearningPathsService } from './named-learning-paths.service';

describe('NamedLearningPathsService', () => {
  let service: NamedLearningPathsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NamedLearningPathsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
