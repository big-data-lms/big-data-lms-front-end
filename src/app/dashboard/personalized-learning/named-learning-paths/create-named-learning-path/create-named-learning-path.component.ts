import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap, takeUntil} from 'rxjs/operators';
import {NamedLearningPathsService} from '../named-learning-paths.service';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {DashboardService} from '../../../dashboard.service';

@Component({
  selector: 'app-create-named-learning-path',
  templateUrl: './create-named-learning-path.component.html',
  styleUrls: ['./create-named-learning-path.component.scss']
})
export class CreateNamedLearningPathComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<any> = new Subject();

  constructor(
    private namedLearningPathsService: NamedLearningPathsService,
    private router: Router,
    private snackBarService: SnackBarService,
    private dashboardService: DashboardService
  ) {
    this.dashboardService.title$.next('Создание именованной траектории');
  }

  ngOnInit(): void {
    this.createNamedLearningPathOnEmit();
  }

  createNamedLearningPathOnEmit() {
    this.namedLearningPathsService.namedLearningPathGeneralFormEmitter$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap(form => this.namedLearningPathsService.createLearningPath(form))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Траектория создана');
        this.router.navigateByUrl('/dashboard/learning_paths');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
