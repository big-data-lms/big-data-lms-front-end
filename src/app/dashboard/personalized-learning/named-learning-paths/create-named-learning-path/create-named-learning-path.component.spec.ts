import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNamedLearningPathComponent } from './create-named-learning-path.component';

describe('CreateNamedLearningPathComponent', () => {
  let component: CreateNamedLearningPathComponent;
  let fixture: ComponentFixture<CreateNamedLearningPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNamedLearningPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNamedLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
