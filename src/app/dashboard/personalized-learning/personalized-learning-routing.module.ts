import { Routes} from '@angular/router';
import {NamedLearningPathListComponent} from './named-learning-paths/named-learning-path-list/named-learning-path-list.component';
// tslint:disable-next-line:max-line-length
import {SpecificNamedLearningPathComponent} from './named-learning-paths/specific-named-learning-path/specific-named-learning-path.component';
import {CreateNamedLearningPathComponent} from './named-learning-paths/create-named-learning-path/create-named-learning-path.component';
import {EditNamedLearningPathComponent} from './named-learning-paths/edit-named-learning-path/edit-named-learning-path.component';
import {CreateAdaptiveTestComponent} from './adaptive-tests/create-adaptive-test/create-adaptive-test.component';
import {EditAdaptiveTestComponent} from './adaptive-tests/edit-adaptive-test/edit-adaptive-test.component';
import {ListOfAdaptiveTestsComponent} from './adaptive-tests/list-of-adaptive-tests/list-of-adaptive-tests.component';
import {TestPassingCurrentQuestionComponent} from './adaptive-tests/test-passing-current-question/test-passing-current-question.component';


export const personalizedLearningRoutes: Routes = [
  {path: 'learning_paths', component: NamedLearningPathListComponent},
  {path: 'learning_paths/create', component: CreateNamedLearningPathComponent},
  {path: 'learning_paths/:id/edit', component: EditNamedLearningPathComponent},
  {path: 'learning_paths/:id', component: SpecificNamedLearningPathComponent},
  {path: 'adaptive_tests', component: ListOfAdaptiveTestsComponent},
  {path: 'adaptive_tests/create', component: CreateAdaptiveTestComponent},
  {path: 'adaptive_tests/:id/edit', component: EditAdaptiveTestComponent},
  {path: 'test_passings/:id', component: TestPassingCurrentQuestionComponent}
];
