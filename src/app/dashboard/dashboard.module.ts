import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import {MaterialModule} from './material.module';
import {CoreModule} from '../core/core.module';
import {LmsCoreModule} from './lms-core/lms-core.module';
import {PersonalizedLearningModule} from './personalized-learning/personalized-learning.module';


@NgModule({
  declarations: [DashboardLayoutComponent],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    DashboardRoutingModule,
    LmsCoreModule,
    PersonalizedLearningModule
  ]
})
export class DashboardModule { }
