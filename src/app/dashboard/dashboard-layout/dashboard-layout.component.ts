import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {AuthService} from '../../auth/auth.service';
import {DashboardService} from '../dashboard.service';
import {map, takeUntil, tap} from 'rxjs/operators';
import {MatSidenav} from '@angular/material/sidenav';
import {Observable, Subject} from 'rxjs';
import {UserSubject} from '../../auth/classes';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements AfterViewChecked, OnDestroy {

  destroy$: Subject<any> = new Subject<any>();
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(['(max-width: 750px)'])
    .pipe(
      takeUntil(this.destroy$),
      map(result => result.matches)
    );
  @ViewChild('drawer') drawer: MatSidenav;
  title$: Observable<string>;
  userInfo$: Subject<UserSubject>;


  constructor(private breakpointObserver: BreakpointObserver,
              private authService: AuthService,
              public dashboardService: DashboardService,
              private ref: ChangeDetectorRef,
              private titleService: Title) {
    this.title$ = this.dashboardService.title$
      .pipe(
        tap(title => this.titleService.setTitle(title))
      );
    this.userInfo$ = this.authService.userSubject$;
  }

  ngAfterViewChecked() {
   this.ref.detectChanges();
  }

  logout() {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}
