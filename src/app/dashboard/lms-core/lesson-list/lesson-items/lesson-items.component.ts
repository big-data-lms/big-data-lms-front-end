import { Component, OnInit } from '@angular/core';
import {Lesson} from '../../classes';
import {LessonListService} from '../lesson-list.service';
import {map, switchMap, take} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {DashboardService} from '../../../dashboard.service';

@Component({
  selector: 'app-lesson-items',
  templateUrl: './lesson-items.component.html',
  styleUrls: ['./lesson-items.component.scss']
})
export class LessonItemsComponent implements OnInit {

  lessonList: Lesson[] = [] as Lesson[];

  courseId$: Observable<number> = this.route.params
      .pipe(
        map(params => params.courseId)
      );

  constructor(private lessonListService: LessonListService,
              private dashboardService: DashboardService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Список уроков');
    this.getLessons();
  }

  getLessons() {
    this.courseId$
      .pipe(
        take(1),
        switchMap(courseId => this.lessonListService.getLessons(courseId))
      )
      .subscribe(lessons => this.lessonList = lessons);
  }

}
