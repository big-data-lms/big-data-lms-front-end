import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonItemsComponent } from './lesson-items.component';

describe('LessonItemsComponent', () => {
  let component: LessonItemsComponent;
  let fixture: ComponentFixture<LessonItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
