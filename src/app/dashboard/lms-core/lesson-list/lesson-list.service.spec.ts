import { TestBed } from '@angular/core/testing';

import { LessonListService } from './lesson-list.service';

describe('LessonListService', () => {
  let service: LessonListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LessonListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
