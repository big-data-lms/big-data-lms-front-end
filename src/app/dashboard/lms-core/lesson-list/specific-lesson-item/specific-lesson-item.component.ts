import {Component, Input, OnInit} from '@angular/core';
import {Lesson} from '../../classes';
import {LessonListService} from '../lesson-list.service';
import {take} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {FileDownloadService} from '../../../../core/services/file-download.service';

@Component({
  selector: 'app-specific-lesson-item',
  templateUrl: './specific-lesson-item.component.html',
  styleUrls: ['./specific-lesson-item.component.scss']
})
export class SpecificLessonItemComponent implements OnInit {

  @Input() lesson: Lesson;
  @Input() index: number;

  form = this.formBuilder.group({
    drawio_file: ['']
  });

  constructor(private lessonListService: LessonListService,
              private snackBarService: SnackBarService,
              private fileDownloadService: FileDownloadService,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.lessonListService.passLesson(this.lesson.id, this.form.value.drawio_file)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Вы начали проходить этот урок');
        this.router.navigateByUrl('/dashboard/passed_lessons');
      });
  }

  onFileChange($event, fileField: string) {
    this.fileDownloadService.onFileChange($event, fileField, this.form);
  }

  onFileDownload() {
    this.fileDownloadService.downloadFileFromForm(this.form, 'drawio_file', 'yaml');
  }

}
