import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificLessonItemComponent } from './specific-lesson-item.component';

describe('SpecificLessonItemComponent', () => {
  let component: SpecificLessonItemComponent;
  let fixture: ComponentFixture<SpecificLessonItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificLessonItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificLessonItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
