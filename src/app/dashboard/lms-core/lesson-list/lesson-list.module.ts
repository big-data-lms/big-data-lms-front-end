import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonItemsComponent } from './lesson-items/lesson-items.component';
import { SpecificLessonItemComponent } from './specific-lesson-item/specific-lesson-item.component';
import {MaterialModule} from '../../material.module';
import {CoreModule} from '../../../core/core.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from "@angular/forms";

const components = [
  LessonItemsComponent,
  SpecificLessonItemComponent
];

@NgModule({
  declarations: [...components],
    imports: [
        CommonModule,
        MaterialModule,
        CoreModule,
        RouterModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
  exports: [
    ...components
  ]
})
export class LessonListModule { }
