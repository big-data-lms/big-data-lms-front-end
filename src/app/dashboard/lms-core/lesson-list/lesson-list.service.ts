import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Lesson, PassedLesson} from '../classes';

@Injectable({
  providedIn: 'root'
})
export class LessonListService {

  constructor(private http: HttpClient) { }

  public getLessons(courseId: number): Observable<Lesson[]> {
    const params = new HttpParams({
      fromObject: {
        course: String(courseId)
      }
    });
    return this.http.get<Lesson[]>(`/api/lessons`, {params});
  }

  public passLesson(lessonId: number, drawioFileContent?): Observable<PassedLesson> {
    const formData = new FormData();
    formData.append('lesson_id', String(lessonId));
    if (drawioFileContent) {
      formData.append('drawio_file', drawioFileContent);
    }

    return this.http.post<PassedLesson>('/api/passed_lessons/', formData);
  }

}
