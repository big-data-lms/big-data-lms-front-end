import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecificCourseItemComponent } from './specific-course-item/specific-course-item.component';
import { CourseItemsComponent } from './course-items/course-items.component';
import {MaterialModule} from '../../material.module';
import {RouterModule} from '@angular/router';
import {CoreModule} from '../../../core/core.module';
import {HttpClientModule} from '@angular/common/http';

const componentList = [
  SpecificCourseItemComponent,
  CourseItemsComponent
];

@NgModule({
  declarations: [...componentList],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    ...componentList
  ]
})
export class CourseListModule { }
