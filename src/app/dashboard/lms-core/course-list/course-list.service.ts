import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Course} from '../classes';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CourseListService {

  constructor(private http: HttpClient) { }

  public getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>('/api/courses');
  }
}
