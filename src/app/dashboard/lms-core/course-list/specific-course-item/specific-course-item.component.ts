import {Component, Input, OnInit} from '@angular/core';
import {Course} from '../../classes';

@Component({
  selector: 'app-specific-course-item',
  templateUrl: './specific-course-item.component.html',
  styleUrls: ['./specific-course-item.component.scss']
})
export class SpecificCourseItemComponent implements OnInit {

  @Input() course: Course;

  constructor() { }

  ngOnInit(): void {
  }

}
