import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificCourseItemComponent } from './specific-course-item.component';

describe('SpecificCourseItemComponent', () => {
  let component: SpecificCourseItemComponent;
  let fixture: ComponentFixture<SpecificCourseItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificCourseItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificCourseItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
