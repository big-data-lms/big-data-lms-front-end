import { Component, OnInit } from '@angular/core';
import {CourseListService} from '../course-list.service';
import {take} from 'rxjs/operators';
import {Course} from '../../classes';
import {DashboardService} from '../../../dashboard.service';

@Component({
  selector: 'app-course-items',
  templateUrl: './course-items.component.html',
  styleUrls: ['./course-items.component.scss']
})
export class CourseItemsComponent implements OnInit {

  courseList: Course[] = [] as Course[];

  constructor(private courseListService: CourseListService,
              private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Список курсов');
    this.getCourseItems();
  }

  getCourseItems() {
    this.courseListService.getCourses()
      .pipe(
        take(1)
      )
      .subscribe(courseList => this.courseList = courseList);
  }

}
