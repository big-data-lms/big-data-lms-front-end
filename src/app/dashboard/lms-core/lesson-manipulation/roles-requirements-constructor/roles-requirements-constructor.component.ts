import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {LessonManipulationService} from '../lesson-manipulation.service';
import {take} from 'rxjs/operators';
import {MatDialogRef} from '@angular/material/dialog';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {FileDownloadService} from '../../../../core/services/file-download.service';

@Component({
  selector: 'app-roles-requirements-constructor',
  templateUrl: './roles-requirements-constructor.component.html',
  styleUrls: ['./roles-requirements-constructor.component.scss']
})
export class RolesRequirementsConstructorComponent implements OnInit {

  form = this.formBuilder.group({
    roles: this.formBuilder.array([
      this.newRole()
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<RolesRequirementsConstructorComponent>,
    private formBuilder: FormBuilder,
    private lessonManipulationService: LessonManipulationService,
    private snackBarService: SnackBarService,
    private fileDownloadService: FileDownloadService
  ) { }

  ngOnInit(): void {
  }

  get roles(): FormArray {
    return this.form.get('roles') as FormArray;
  }

  newRole() {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(256)]],
      src: ['', [Validators.maxLength(256)]],
      version: ['', [Validators.maxLength(256)]],
      scm: ['', [Validators.maxLength(256)]],
    });
  }

  addRole() {
    this.roles.push(this.newRole());
  }

  removeRole(index: number) {
    this.roles.removeAt(index);
  }

  prepareForDownloadGeneratedFile() {
    return this.lessonManipulationService.getGeneratedFile(this.form.value, 'roles_requirements')
      .pipe(
        take(1)
      );
  }

  downloadGeneratedFile() {
    this.prepareForDownloadGeneratedFile()
      .subscribe(generatedFile => {
        this.fileDownloadService.downloadFile(`requirements.yml`, generatedFile);
      });
  }

  onSubmit() {
    this.prepareForDownloadGeneratedFile()
      .subscribe(generatedFile => {
        const fileObj = new File([generatedFile], 'requirements.yaml');
        this.lessonManipulationService.rolesRequirementsFile$.next(fileObj);
        this.snackBarService.openSnackBar('Сгенерированные файлы загружены, окно можно закрывать');
      });
  }

  onRoleArchiveFileChange($event, roleIndex: number) {
    if ($event.target.files.length > 0) {
      const file = $event.target.files[0];
      this.lessonManipulationService.uploadRoleArchive(file)
        .pipe(
          take(1)
        )
        .subscribe(uploadedFile => {
          this.roles.at(roleIndex).patchValue({
            src: uploadedFile.role_archive_file
          });
        });
    }
  }
}
