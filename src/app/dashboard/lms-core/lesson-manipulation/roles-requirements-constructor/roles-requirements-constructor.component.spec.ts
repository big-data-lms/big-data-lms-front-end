import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesRequirementsConstructorComponent } from './roles-requirements-constructor.component';

describe('RolesRequirementsConstructorComponent', () => {
  let component: RolesRequirementsConstructorComponent;
  let fixture: ComponentFixture<RolesRequirementsConstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesRequirementsConstructorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesRequirementsConstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
