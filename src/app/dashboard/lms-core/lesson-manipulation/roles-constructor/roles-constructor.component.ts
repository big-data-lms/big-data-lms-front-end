import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {AbstractControl, FormArray, FormBuilder, Validators} from '@angular/forms';
import {LessonManipulationService} from '../lesson-manipulation.service';
import {take} from 'rxjs/operators';
import {FileDownloadService} from '../../../../core/services/file-download.service';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-roles-constructor',
  templateUrl: './roles-constructor.component.html',
  styleUrls: ['./roles-constructor.component.scss']
})
export class RolesConstructorComponent implements OnInit {

  form = this.formBuilder.group({
    sets: this.formBuilder.array([
      this.newSetOfInstances()
    ])
  });

  constructor(public dialogRef: MatDialogRef<RolesConstructorComponent>,
              private formBuilder: FormBuilder,
              private lessonManipulationService: LessonManipulationService,
              private snackBarService: SnackBarService,
              private fileDownloadService: FileDownloadService) { }

  ngOnInit(): void {
  }

  newTagValue() {
    return this.formBuilder.group({
      value: ['', [Validators.maxLength(256)]],
    });
  }

  addTagValue(index: number) {
    const currentSet = this.sets.at(index);
    const currentTagsArray = this.getAllTagsArrayFromSet(currentSet);
    currentTagsArray.push(this.newTagValue());
  }

  removeTagValue(setIndex: number, tagValueIndex: number) {
    const currentSet = this.sets.at(setIndex);
    const currentTagsArray = this.getAllTagsArrayFromSet(currentSet);
    currentTagsArray.removeAt(tagValueIndex);
  }

  newSetOfInstances() {
    return this.formBuilder.group({
      count_tag_value: ['', [Validators.required, Validators.maxLength(256)]],
      count: [1, [Validators.required, Validators.min(1), Validators.max(100)]],
      type: ['', [Validators.required, Validators.maxLength(100)]],
      all_tags: this.formBuilder.array([
        this.newTagValue()
      ])
    });
  }

  get sets(): FormArray {
    return this.form.get('sets') as FormArray;
  }

  addSetOfInstances() {
    this.sets.push(this.newSetOfInstances());
  }

  removeSetOfInstances(index: number) {
    this.sets.removeAt(index);
  }

  getAllTagsArrayFromSet(set: AbstractControl): FormArray {
    return set.get('all_tags') as FormArray;
  }

  prepareForDownloadGeneratedTemplate(templateName: string) {
    const formValue = JSON.parse(JSON.stringify(this.form.value)); // deep copy form value

    for (const set of formValue.sets) {
      set.count_tag_name = `${this.lessonManipulationService.randomUUID}__${set.count_tag_value}`;
      for (const tag of set.all_tags) {
        tag.name = `${this.lessonManipulationService.randomUUID}__${tag.value}`;
      }
      set.all_tags = set.all_tags.filter(tag => tag.value.trim() !== '');
      set.all_tags.push({
        name: set.count_tag_name,
        value: set.count_tag_value
      });
    }

    return this.lessonManipulationService.getGeneratedFile(formValue, templateName)
      .pipe(
        take(1)
      );
  }

  downloadGeneratedTemplate(templateName: string) {

    this.prepareForDownloadGeneratedTemplate(templateName)
      .subscribe(generatedFile => {
        this.fileDownloadService.downloadFile(`${templateName}.yaml`, generatedFile);
      });

  }

  onSubmit() {
    this.prepareForDownloadGeneratedTemplate('variables')
      .subscribe(generatedFile => {
        const fileObj = new File([generatedFile], 'variables.yaml');
        this.lessonManipulationService.variablesGeneratedFile$.next(fileObj);
      });

    this.prepareForDownloadGeneratedTemplate('roles_manipulation')
      .subscribe(generatedFile => {
        const fileObj = new File([generatedFile], 'roles_manipulation.yaml');
        this.lessonManipulationService.rolesManipulationGeneratedPlaybook$.next(fileObj);
        this.snackBarService.openSnackBar('Файлы подставлены в форму');
      });
  }

}
