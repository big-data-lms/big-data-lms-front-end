import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesConstructorComponent } from './roles-constructor.component';

describe('RolesConstructorComponent', () => {
  let component: RolesConstructorComponent;
  let fixture: ComponentFixture<RolesConstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesConstructorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesConstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
