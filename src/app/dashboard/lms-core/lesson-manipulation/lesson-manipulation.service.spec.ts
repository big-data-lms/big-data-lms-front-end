import { TestBed } from '@angular/core/testing';

import { LessonManipulationService } from './lesson-manipulation.service';

describe('LessonManipulationService', () => {
  let service: LessonManipulationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LessonManipulationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
