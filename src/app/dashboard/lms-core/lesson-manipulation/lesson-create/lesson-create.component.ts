import {Component, OnDestroy, OnInit} from '@angular/core';
import {DashboardService} from '../../../dashboard.service';
import {LessonManipulationService} from '../lesson-manipulation.service';
import {Subject, zip} from 'rxjs';
import {map, switchMap, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-lesson-create',
  templateUrl: './lesson-create.component.html',
  styleUrls: ['./lesson-create.component.scss']
})
export class LessonCreateComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<any> = new Subject();

  constructor(private dashboardService: DashboardService,
              private lessonManipulationService: LessonManipulationService,
              private snackBarService: SnackBarService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Создание урока');
    this.processFormSubmit();
  }

  processFormSubmit() {
    zip(
      this.route.params
        .pipe(
          map(params => params.courseId as number)
        ),
      this.lessonManipulationService.lessonFormEmitter$
    )
      .pipe(
        takeUntil(this.unsubscribe$),
        map(zipped => {
          return {
            courseId: zipped[0],
            lessonForm: zipped[1]
          };
        }),
        switchMap(({courseId, lessonForm}) => this.lessonManipulationService.createLesson({
          course: courseId,
          ...lessonForm
        }))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Урок создан');
        this.router.navigateByUrl('/dashboard');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
