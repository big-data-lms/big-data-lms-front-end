import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LessonManipulationService} from '../lesson-manipulation.service';
import {MatDialog} from '@angular/material/dialog';
import {RolesConstructorComponent} from '../roles-constructor/roles-constructor.component';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {RolesRequirementsConstructorComponent} from '../roles-requirements-constructor/roles-requirements-constructor.component';
import {FileDownloadService} from '../../../../core/services/file-download.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-lesson-form',
  templateUrl: './lesson-form.component.html',
  styleUrls: ['./lesson-form.component.scss']
})
export class LessonFormComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<any> = new Subject();

  timerRegexp = '\\d{2}:\\d{2}:\\d{2}';

  lessonForm: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(80)]],
    description: ['', [Validators.required, Validators.maxLength(500)]],

    roles_requirements_file: ['', [Validators.required]],
    role_management_playbook_file: ['', [Validators.required]],
    instances_provisioning_playbook_file: [''],
    variables_file: ['', [Validators.required]],
    drawio_library_file: [''],

    notification_timer: ['', [Validators.pattern(this.timerRegexp)]],
    stop_instances_timer: ['', [Validators.pattern(this.timerRegexp)]],
    terminate_instances_timer: ['', [Validators.pattern(this.timerRegexp)]]
  });

  constructor(private formBuilder: FormBuilder,
              private lessonManipulationService: LessonManipulationService,
              private fileDownloadService: FileDownloadService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.changeRandomUUID();
    this.onVariablesFileReceive();
    this.onRoleManipulationPlaybookReceive();
    this.onRolesRequirementsFileReceive();
  }

  onSubmit() {
    const value = this.lessonForm.value;

    value.ansible_template = {};

    Object.assign(value.ansible_template, {roles_requirements_file: value.roles_requirements_file});
    Object.assign(value.ansible_template, {role_management_playbook_file: value.role_management_playbook_file});
    Object.assign(value.ansible_template, {variables_file: value.variables_file});
    Object.assign(value.ansible_template, {template_uuid: this.lessonManipulationService.randomUUID});

    if (value.drawio_library_file) {
      Object.assign(value.ansible_template, {drawio_library_file: value.drawio_library_file});
    }

    if (value.instances_provisioning_playbook_file) {
      Object.assign(value.ansible_template, {instances_provisioning_playbook_file: value.instances_provisioning_playbook_file});
    }

    this.lessonManipulationService.lessonFormEmitter$.next(value);
  }

  onFileChange($event, fileField: string) {
    this.fileDownloadService.onFileChange($event, fileField, this.lessonForm);
  }

  onVariablesFileReceive() {
    this.lessonManipulationService.variablesGeneratedFile$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(file => this.lessonForm.patchValue({
        variables_file: file
      }));
  }

  onRoleManipulationPlaybookReceive() {
    this.lessonManipulationService.rolesManipulationGeneratedPlaybook$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(file => this.lessonForm.patchValue({
        role_management_playbook_file: file
      }));
  }

  onRolesRequirementsFileReceive() {
    this.lessonManipulationService.rolesRequirementsFile$
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(file => this.lessonForm.patchValue({
        roles_requirements_file: file
      }));
  }

  changeRandomUUID() {
    this.lessonManipulationService.randomUUID = uuidv4();
  }

  downloadFileFromForm(formFieldName: string) {
    this.fileDownloadService.downloadFileFromForm(this.lessonForm, formFieldName, 'yaml');
  }

  openSetsOfInstancesConstructor() {
    const dialogRef = this.dialog.open(RolesConstructorComponent, {
      height: '97%'
    });
  }

  openRolesRequirementsConstructor() {
    const dialogRef = this.dialog.open(RolesRequirementsConstructorComponent, {
      height: '97%'
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
