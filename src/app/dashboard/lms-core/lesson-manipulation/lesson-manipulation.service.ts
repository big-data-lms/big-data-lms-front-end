import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Lesson, LessonForm, RoleArchive} from '../classes';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class LessonManipulationService {

  public readonly lessonFormEmitter$: Subject<LessonForm> = new Subject<LessonForm>();
  public readonly variablesGeneratedFile$: Subject<any> = new Subject<any>();
  public readonly rolesManipulationGeneratedPlaybook$: Subject<any> = new Subject<any>();
  public readonly rolesRequirementsFile$: Subject<any> = new Subject<any>();
  public randomUUID = uuidv4();

  constructor(private http: HttpClient) { }

  createLesson(lessonForm: LessonForm): Observable<Lesson> {
    const formData = new FormData();
    Object.keys(lessonForm).forEach(key => {
      const value = lessonForm[key];
      if (typeof(value) === 'object') {
        Object.keys(value).forEach(valueKey => formData.append(`${key}.${valueKey}`, value[valueKey]));
      } else {
        formData.append(key, lessonForm[key]);
      }
    });
    return this.http.post<Lesson>('/api/lessons/', formData);
  }

  getGeneratedFile(constructorForm, templateName: string): Observable<any> {
    return this.http.post(`/api/playbooks_constructor/${templateName}/`, constructorForm, {
      responseType: 'text'
    });
  }

  uploadRoleArchive(file): Observable<RoleArchive> {
    const formData = new FormData();
    formData.append('role_archive_file', file);
    return this.http.post<RoleArchive>('/api/roles_archives/', formData);
  }

}
