import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonFormComponent } from './lesson-form/lesson-form.component';
import {MaterialModule} from '../../material.module';
import {ReactiveFormsModule} from '@angular/forms';
import { LessonCreateComponent } from './lesson-create/lesson-create.component';
import { RolesConstructorComponent } from './roles-constructor/roles-constructor.component';
import { RolesRequirementsConstructorComponent } from './roles-requirements-constructor/roles-requirements-constructor.component';

const components = [
  LessonFormComponent,
  LessonCreateComponent,
  RolesConstructorComponent
];

@NgModule({
  declarations: [...components, RolesRequirementsConstructorComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    ...components
  ]
})
export class LessonManipulationModule { }
