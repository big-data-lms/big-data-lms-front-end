import { TestBed } from '@angular/core/testing';

import { PassedLessonsListService } from './passed-lessons-list.service';

describe('PassedLessonsListService', () => {
  let service: PassedLessonsListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PassedLessonsListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
