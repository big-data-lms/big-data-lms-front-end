import {Component, Input, OnInit} from '@angular/core';
import {PassedLesson} from '../../classes';
import {FormBuilder, Validators} from '@angular/forms';
import {PassedLessonsListService} from '../passed-lessons-list.service';
import {take} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';

@Component({
  selector: 'app-manipulate-instances-form',
  templateUrl: './manipulate-instances-form.component.html',
  styleUrls: ['./manipulate-instances-form.component.scss']
})
export class ManipulateInstancesFormComponent implements OnInit {

  @Input() passedLesson: PassedLesson;

  form = this.formBuilder.group({
    status: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder,
              private passedLessonsListService: PassedLessonsListService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.passedLessonsListService.manipulateInstances(this.passedLesson.id, this.form.get('status').value)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Статус изменяется');
      });
  }

}
