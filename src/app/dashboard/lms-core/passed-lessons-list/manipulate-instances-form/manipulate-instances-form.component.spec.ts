import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipulateInstancesFormComponent } from './manipulate-instances-form.component';

describe('ManipulateInstancesFormComponent', () => {
  let component: ManipulateInstancesFormComponent;
  let fixture: ComponentFixture<ManipulateInstancesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManipulateInstancesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManipulateInstancesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
