import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PassedLesson} from '../classes';

@Injectable({
  providedIn: 'root'
})
export class PassedLessonsListService {

  constructor(private http: HttpClient) { }

  public getPassedLessons(studentId: number): Observable<PassedLesson[]> {
    const params = new HttpParams({
      fromObject: {
        student: String(studentId)
      }
    });
    return this.http.get<PassedLesson[]>(`/api/passed_lessons/`, {params});
  }

  public readNotification(id: number): Observable<{}> {
    return this.http.put<{}>(`/api/passed_lessons/${id}/read_notification/`, {});
  }

  public manipulateInstances(id: number, status: string): Observable<{}> {
    return this.http.put<{}>(`/api/passed_lessons/${id}/manipulate_instances/`, {status});
  }

}
