import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificPassedLessonsItemComponent } from './specific-passed-lessons-item.component';

describe('SpecificPassedLessonsItemComponent', () => {
  let component: SpecificPassedLessonsItemComponent;
  let fixture: ComponentFixture<SpecificPassedLessonsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificPassedLessonsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificPassedLessonsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
