import {Component, Input, OnInit} from '@angular/core';
import {PassedLessonsListService} from '../passed-lessons-list.service';
import {take} from 'rxjs/operators';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {PassedLesson} from '../../classes';

@Component({
  selector: 'app-specific-passed-lessons-item',
  templateUrl: './specific-passed-lessons-item.component.html',
  styleUrls: ['./specific-passed-lessons-item.component.scss']
})
export class SpecificPassedLessonsItemComponent implements OnInit {

  @Input() passedLesson: PassedLesson = new PassedLesson();

  constructor(private passedLessonsListService: PassedLessonsListService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
  }

  readNotification() {
    this.passedLessonsListService.readNotification(this.passedLesson.id)
      .pipe(
        take(1)
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Уведомление прочитано');
        this.passedLesson.is_notification_active = false;
      });
  }
}
