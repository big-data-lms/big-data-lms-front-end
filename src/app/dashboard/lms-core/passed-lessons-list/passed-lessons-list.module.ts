import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PassedLessonsItemsComponent } from './passed-lessons-items/passed-lessons-items.component';
import { SpecificPassedLessonsItemComponent } from './specific-passed-lessons-item/specific-passed-lessons-item.component';
import {MaterialModule} from '../../material.module';
import {CoreModule} from '../../../core/core.module';
import { ManipulateInstancesFormComponent } from './manipulate-instances-form/manipulate-instances-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [PassedLessonsItemsComponent, SpecificPassedLessonsItemComponent, ManipulateInstancesFormComponent],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class PassedLessonsListModule { }
