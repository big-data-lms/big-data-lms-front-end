import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassedLessonsItemsComponent } from './passed-lessons-items.component';

describe('PassedLessonsItemsComponent', () => {
  let component: PassedLessonsItemsComponent;
  let fixture: ComponentFixture<PassedLessonsItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassedLessonsItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassedLessonsItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
