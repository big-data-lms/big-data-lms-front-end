import { Component, OnInit } from '@angular/core';
import {DashboardService} from '../../../dashboard.service';
import {PassedLessonsListService} from '../passed-lessons-list.service';
import {AuthService} from '../../../../auth/auth.service';
import {switchMap, take} from 'rxjs/operators';
import {PassedLesson} from '../../classes';

@Component({
  selector: 'app-passed-lessons-items',
  templateUrl: './passed-lessons-items.component.html',
  styleUrls: ['./passed-lessons-items.component.scss']
})
export class PassedLessonsItemsComponent implements OnInit {

  passedLessonsList: PassedLesson[] = [];

  constructor(private dashboardService: DashboardService,
              private authService: AuthService,
              private passedLessonsListService: PassedLessonsListService) { }

  ngOnInit(): void {
    this.dashboardService.title$.next('Список прохождений');
    this.getPassedLessonItems();
  }

  getPassedLessonItems() {
    this.authService.userSubject$
      .pipe(
        take(1),
        switchMap(user => this.passedLessonsListService.getPassedLessons(user.id))
      )
      .subscribe(passedLessons => this.passedLessonsList = passedLessons);
  }

}
