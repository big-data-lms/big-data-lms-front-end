import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CourseListModule} from './course-list/course-list.module';
import {LessonListModule} from './lesson-list/lesson-list.module';
import {CourseManipulationModule} from './course-manipulation/course-manipulation.module';
import {LessonManipulationModule} from './lesson-manipulation/lesson-manipulation.module';
import {PassedLessonsListModule} from './passed-lessons-list/passed-lessons-list.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CourseListModule,
    LessonListModule,
    CourseManipulationModule,
    LessonManipulationModule,
    PassedLessonsListModule
  ]
})
export class LmsCoreModule { }
