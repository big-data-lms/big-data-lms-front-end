import { Routes, RouterModule } from '@angular/router';
import {CourseItemsComponent} from './course-list/course-items/course-items.component';
import {CourseCreateComponent} from './course-manipulation/course-create/course-create.component';
import {LessonItemsComponent} from './lesson-list/lesson-items/lesson-items.component';
import {LessonCreateComponent} from './lesson-manipulation/lesson-create/lesson-create.component';
import {PassedLessonsItemsComponent} from './passed-lessons-list/passed-lessons-items/passed-lessons-items.component';


export const lmsCoreRoutes: Routes = [
  {path: '', component: CourseItemsComponent},
  {path: 'courses/create', component: CourseCreateComponent},
  {path: 'courses/:courseId/lessons', component: LessonItemsComponent},
  {path: 'courses/:courseId/create_lesson', component: LessonCreateComponent},
  {path: 'passed_lessons', component: PassedLessonsItemsComponent}
];
