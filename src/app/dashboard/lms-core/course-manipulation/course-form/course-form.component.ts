import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CourseManipulationService} from '../course-manipulation.service';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {

  timerRegexp = '\\d{2}:\\d{2}:\\d{2}';

  courseForm: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(80)]],
    description: ['', [Validators.required, Validators.maxLength(500)]],
    notification_timer: ['', [Validators.required, Validators.pattern(this.timerRegexp)]],
    stop_instances_timer: ['', [Validators.required, Validators.pattern(this.timerRegexp)]],
    terminate_instances_timer: ['', [Validators.required, Validators.pattern(this.timerRegexp)]]
  });

  constructor(private formBuilder: FormBuilder,
              private courseManipulationService: CourseManipulationService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.courseManipulationService.courseFormEmitter$.next(this.courseForm.value);
  }
}
