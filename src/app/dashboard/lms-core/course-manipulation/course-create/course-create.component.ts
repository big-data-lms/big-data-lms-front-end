import {Component, OnDestroy, OnInit} from '@angular/core';
import {CourseManipulationService} from '../course-manipulation.service';
import {Subject} from 'rxjs';
import {switchMap, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {SnackBarService} from '../../../../core/services/snack-bar.service';
import {DashboardService} from '../../../dashboard.service';

@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.scss']
})
export class CourseCreateComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<any> = new Subject();

  constructor(private courseManipulationService: CourseManipulationService,
              private router: Router,
              private snackBarService: SnackBarService,
              private dashboardService: DashboardService
  ) {
    this.dashboardService.title$.next('Создание курса');
  }

  ngOnInit(): void {
    this.courseManipulationService.courseFormEmitter$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap(courseForm => this.courseManipulationService.createCourse(courseForm))
      )
      .subscribe(_ => {
        this.snackBarService.openSnackBar('Курс создан');
        this.router.navigateByUrl('/dashboard');
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}
