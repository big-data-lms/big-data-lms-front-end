import { TestBed } from '@angular/core/testing';

import { CourseManipulationService } from './course-manipulation.service';

describe('CourseManipulationService', () => {
  let service: CourseManipulationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourseManipulationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
