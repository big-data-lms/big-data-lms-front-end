import { Injectable } from '@angular/core';
import {Course, CourseForm} from '../classes';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CourseManipulationService {

  public readonly courseFormEmitter$: Subject<CourseForm> = new Subject<CourseForm>();

  constructor(private http: HttpClient) { }

  public createCourse(course: CourseForm): Observable<Course> {
    return this.http.post<Course>('/api/courses/', course);
  }

}
