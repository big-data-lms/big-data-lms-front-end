// tslint:disable:variable-name
// tslint:disable:no-use-before-declare


import {UserSubject} from '../../auth/classes';

export class Course {
  constructor(public id: number = 0,
              public name: string = '',
              public description: string = '',
              public notification_timer: string = '',
              public stop_instances_timer: string = '',
              public terminate_instances_timer: string = '',
              public teacher: UserSubject = new UserSubject()
  ) {}
}

export type CourseForm = Omit<Course, 'id' | 'teacher'>;

export class LessonForm {
  id: number;
  name: string;
  description: string;
  course: number;

  role_management_playbook: string;
  role_url: string;
  role_archive: string;
  instances_management_playbook: string;

  notification_timer: string;
  stop_instances_timer: string;
  terminate_instances_timer: string;
}

export class Lesson {
  id: number;
  name: string;
  description: string;
  course: number;
  ansible_template: AnsibleTemplate;
  notification_timer: string;
  stop_instances_timer: string;
  terminate_instances_timer: string;
}

export class AnsibleTemplate {
  id: number;
  description: string;
  role_management_playbook_file: string;
  role_archive_file: string;
  instances_management_playbook_file: string;
  variables_file: string;
  drawio_library_file: string;
}

export class LessonInsidePassedLesson {
  constructor(public id: number = 0,
              public name: string = '') {
  }
}

export class PassedLesson {
  constructor(public id: number = 0,
              public student_id: number = 0,
              public lesson: LessonInsidePassedLesson = new LessonInsidePassedLesson(),
              public start_datetime: string = '',
              public provisioning_return_code: number = -1,
              public configuration_return_code: number = -1,
              public provisioning_logs: string = '',
              public configuration_logs: string = '',
              public key_file: string = '',
              public status: string = '',
              public is_notification_active: boolean = false
  ) {}
}

export class RoleArchive {
  constructor(
    public id: number = 0,
    public role_archive_file: string = ''
  ) {}
}
