import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './core/guards/auth.guard';
import {RootGuard} from './core/guards/root.guard';
import {RootComponent} from './root/root.component';
import {DashboardGuard} from './core/guards/dashboard-guard.service';


const routes: Routes = [
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule),
    canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(mod => mod.DashboardModule),
    canActivate: [DashboardGuard], canActivateChild: [DashboardGuard]},
  { path: '', pathMatch: 'full', component: RootComponent, canActivate: [RootGuard] }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
