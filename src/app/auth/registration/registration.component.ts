import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SnackBarService} from '../../core/services/snack-bar.service';
import {Title} from '@angular/platform-browser';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  regForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email, Validators.maxLength(254)]],
    fio: ['', [Validators.required,
      Validators.pattern(/^ *([а-яА-Я]|[^\W_\d]|-)+( +([а-яА-Я]|[^\W_\d]|-)+){0,} *$/),
      Validators.maxLength(100)]],
    aws_access_key_id: ['', [Validators.required, Validators.maxLength(100)]],
    aws_secret_access_key: ['', [Validators.required, Validators.maxLength(100)]],
    aws_session_token: ['', [Validators.maxLength(500)]],
    password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
    repeatPassword: ['', Validators.required]
  }, {
    validator: this.validateAreEqual('password', 'repeatPassword')
  });

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              private snackBarService: SnackBarService,
              private titleService: Title) {
    this.titleService.setTitle('Регистрация');
  }

  ngOnInit() {
    this.authService.title$.next('Регистрация');
  }

  validateAreEqual(first: string, second: string) {
    return (group: FormGroup) => {
      const firstControl = group.controls[first];
      const secondControl = group.controls[second];

      if (firstControl.value !== secondControl.value) {
        return {
          notEqual: true
        };
      }

    };
  }

  onSubmit() {
    this.regForm.disable();
    this.authService.register(this.regForm.value)
      .pipe(
        take(1)
      )
      .subscribe(
        () => {
          this.router.navigate(['/auth'], {queryParams: {isAfterRegistration: true}});
        },
        error => {
          console.log(error);
          if (error.status === 400) {
            if (error.error.email[0] && error.error.email[0] === 'This field must be unique.') {
              this.snackBarService.openSnackBar('Пользователь с таким email уже существует');
            } else {
              this.snackBarService.openSnackBar('Неверные данные', 'Неясно');
            }
          } else {
            this.snackBarService.openSnackBar('Что-то пошло не так, попробуйте позже');
          }
          this.regForm.enable();
        });
  }
}
