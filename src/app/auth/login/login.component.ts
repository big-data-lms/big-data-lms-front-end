import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SnackBarService} from '../../core/services/snack-bar.service';
import {Title} from '@angular/platform-browser';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private snackBarService: SnackBarService,
              private titleService: Title) {
    this.route.queryParams
      .pipe(
        take(1)
      )
      .subscribe(params => {
      if (params.isAfterRegistration && params.isAfterRegistration === 'true') {
        this.snackBarService.openSnackBar('Вы успешно зарегистрированы, можете входить');
      }
    });
    this.titleService.setTitle('Вход');
  }

  ngOnInit() {
    this.authService.title$.next('Вход');
  }

  onSubmit() {
    this.loginForm.disable();
    this.authService
      .login(this.loginForm.value)
      .pipe(
        take(1)
      )
      .subscribe(
        () => {
          this.loginForm.enable();
          this.router.navigate(['/']);
        },
        error => {
          this.loginForm.enable();
          console.log(error);

          let errorMsg: string;
          if (error.status === 401) {
            errorMsg = 'Неверный логин или пароль';
          } else if (error.status === 0 && error.name === 'HttpErrorResponse') {
            errorMsg = 'Отсутствует подключение к серверу, попробуйте позже';
          } else {
            errorMsg = 'Произошла ошибка, попробуйте позже';
          }
          this.snackBarService.openSnackBar(errorMsg);
        });
  }
}
