import {AfterContentInit, AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit, AfterViewChecked {
  title$: Subject<string>;

  constructor(private authService: AuthService,
              private ref: ChangeDetectorRef) {
    this.title$ = this.authService.title$;
  }

  ngAfterViewChecked() {
   this.ref.detectChanges();
  }

  ngOnInit() {
  }

}
