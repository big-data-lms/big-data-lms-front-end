// tslint:disable:variable-name
// tslint:disable:no-use-before-declare


export class UserSubject {
  id: number;
  fio: string;

  constructor(options = {
    id: 0,
    fio: ''
  }) {
    Object.assign(this, options);
  }
}

export class AuthTokenRequest {
  username: string;
  password: string;
}

export class AuthTokenResponse {
  access: string;
  refresh: string;
}

export class UserRegistrationRequest {
  email: string;
  fio: string;
  aws_access_key_id: string;
  aws_secret_access_key: string;
  aws_session_token: string;
  password: string;
  repeatPassword: string;
}

export class UserRegistrionResponse {
  id: number;
  email: string;
  fio: string;
  aws_access_key_id: string;
  aws_secret_access_key: string;
  aws_session_token: string;
  password: string;
}
