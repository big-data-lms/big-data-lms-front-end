import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {tap} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {parseJwt} from '../core/helper';
import {
  AuthTokenRequest,
  AuthTokenResponse,
  UserRegistrationRequest,
  UserRegistrionResponse,
  UserSubject
} from './classes';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _title$: Subject<string> = new BehaviorSubject('');
  private _accessToken: string = localStorage.getItem('auth-token');
  private _refreshToken: string = localStorage.getItem('refresh-token');
  private _userSubject$: Subject<UserSubject> = new BehaviorSubject(new UserSubject());

  constructor(private http: HttpClient,
              private router: Router) {
    this.setUserInfo();
  }

  register(user: UserRegistrationRequest): Observable<UserRegistrionResponse> {
    return this.http.post<UserRegistrionResponse>('/api/users/', user);
  }

  login(user: AuthTokenRequest): Observable<AuthTokenResponse> {
    return this.http.post<AuthTokenResponse>('/api/auth/token/', user)
      .pipe(
        tap(
          ({access, refresh}) => {
            localStorage.setItem('auth-token', access);
            localStorage.setItem('refresh-token', refresh);
            this.accessToken = access;
            this.refreshToken = refresh;
            this.setUserInfo();
          }
        )
      );
  }

  loginUsingRefreshToken(): Observable<{ access: string }> {
    const body = new HttpParams()
      .set('refresh', this.refreshToken);
    return this.http.post<AuthTokenResponse>
    (
      '/api/auth/token/refresh/',
      body.toString(), {
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .pipe(
        tap(
          ({access}) => {
            localStorage.setItem('auth-token', access);
            this.accessToken = access;
            this.setUserInfo();
          }
        )
      );
  }

  isAuthenticated(): boolean {
    if (this.refreshToken) {
      const refreshParsed = parseJwt(this.refreshToken) || {exp: 0};
      return !!this.accessToken && Date.now() / 1000 < +refreshParsed.exp;
    }
    return !!this.accessToken;
  }

  logout() {
    this.accessToken = null;
    this.refreshToken = null;
    localStorage.clear();
    this.router.navigateByUrl('/');
  }

  setUserInfo(access: string = this.accessToken) {
    const parsedToken = parseJwt(access) || new UserSubject();
    this.userSubject$.next(new UserSubject({
      fio: parsedToken.fio,
      id: parsedToken.user_id
    }));
  }

  get title$(): Subject<string> {
    return this._title$;
  }

  get accessToken(): string {
    return this._accessToken;
  }

  set accessToken(value: string) {
    this._accessToken = value;
  }

  get refreshToken(): string {
    return this._refreshToken;
  }

  set refreshToken(value: string) {
    this._refreshToken = value;
  }

  get userSubject$(): Subject<UserSubject> {
    return this._userSubject$;
  }
}
